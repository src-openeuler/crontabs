%global snap_release 20190603
Name:           crontabs
Version:        1.11
Release:        24
Summary:        Root crontab files used to schedule the execution of programs
License:        Public Domain and GPLv2+
URL:            https://github.com/cronie-crond/crontabs
Source0:        https://github.com/cronie-crond/crontabs/releases/download/%{name}-%{snap_release}/%{name}-%{version}-%{snap_release}git.tar.gz
BuildArch:      noarch
Requires:       sed
Recommends:     cronie

%description
A crontab file contains instructions to the cron daemon
of the general form: 'run this command at  this  time  on
this  date'.   Each  user has their own crontab, and com-
mands in any given crontab will be executed  as  the  user
who  owns  the  crontab.

%package_help

%prep
%autosetup -p1

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/cron.{hourly,daily,weekly,monthly}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man4/

install -m 644 ./crontab %{buildroot}%{_sysconfdir}/crontab
install -m 755 ./run-parts %{buildroot}%{_bindir}/run-parts
install -m 644 ./*.4 %{buildroot}%{_mandir}/man4/

mkdir -p %{buildroot}%{_sysconfdir}/sysconfig/
touch %{buildroot}%{_sysconfdir}/sysconfig/run-parts

%files
%license COPYING
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/{crontab,sysconfig/run-parts}
%dir %{_sysconfdir}/cron.{hourly,daily,weekly,monthly}
%{_bindir}/run-parts

%files help
%{_mandir}/man4/*

%changelog
* Wed Jun 12 2024 zhangruifang <zhangruifang@h-partners.com> - 1.11-24 
- License compliance rectification

* Tue Oct 25 2022 zhangruifang <zhangruifang1@h-partners.com> - 1.11-23
- Rebuild for next release

* Tue Sep 8 2020 wangchen<wangchen137@huawei.com> - 1.11-22
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: modify the URL of Source0

* Fri Oct 11 2019 jiangchuangang<jiangchuangang@huawei.com> - 1.11-21
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update tarball

* Thu Sep 26 2019 shenyangyang<shenyangyang4@huawei.com> - 1.11-20
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Tue Aug 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.11-19
- Package Init
